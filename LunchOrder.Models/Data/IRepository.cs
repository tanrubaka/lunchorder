﻿using System.Linq;

namespace LunchOrder.Models.Data
{
    public interface IRepository<T>
        where T : class, new()
    {
        IQueryable<T> GetAll();
        void UpdateProperty(T obj, string property);
        void Attach(T obj);
        void Delete(T obj);
        void Add(T obj);
    }
}