﻿using LunchOrder.Models.DomainObjects;

namespace LunchOrder.Models.Data
{
    public interface IUnitOfWork
    {
        IRepository<Food> Foods { get; }
        IRepository<Order> Orders { get; }
        IRepository<OrderItem> OrderItems { get; }

        bool SaveChanges();
        void Discard();
    }
}