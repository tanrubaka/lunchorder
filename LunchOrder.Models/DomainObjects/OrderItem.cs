﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LunchOrder.Models.DomainObjects
{
    public class OrderItem
    {
        public int Id { get; set; }

        public int Qty { get; set; }

        public double Price { get; set; }

        public int FoodId { get; set; }
        [ForeignKey(nameof(FoodId))]
        public Food Food { get; set; }

        public int OrderId { get; set; }
        [ForeignKey(nameof(OrderId))]
        public Order Order { get; set; }
    }
}