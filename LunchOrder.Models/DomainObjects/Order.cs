﻿using System;
using System.Collections.ObjectModel;

namespace LunchOrder.Models.DomainObjects
{
    public class Order
    {
        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public double Amount { get; set; }

        public ObservableCollection<OrderItem> Items { get; set; } = new ObservableCollection<OrderItem>();
    }
}