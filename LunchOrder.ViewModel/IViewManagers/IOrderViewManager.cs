﻿using System.Collections.Generic;
using LunchOrder.ViewModel.Edits;
using LunchOrder.ViewModel.Views;

namespace LunchOrder.ViewModel.IViewManagers
{
    public interface IOrderViewManager
    {
        OrderEdit GetEdit(int? id);
        IEnumerable<OrderView> GetAll();
    }
}