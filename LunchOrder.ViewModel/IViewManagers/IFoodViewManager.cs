﻿using System.Collections.Generic;
using LunchOrder.ViewModel.Views;

namespace LunchOrder.ViewModel.IViewManagers
{
    public interface IFoodViewManager
    {
        IEnumerable<FoodView> GetAll();
    }
}