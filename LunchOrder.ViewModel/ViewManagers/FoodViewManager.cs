﻿using System.Collections.Generic;
using System.Linq;
using LunchOrder.DAL.IManagers;
using LunchOrder.ViewModel.IViewManagers;
using LunchOrder.ViewModel.Views;

namespace LunchOrder.ViewModel.ViewManagers
{
    public class FoodViewManager : IFoodViewManager
    {
        private readonly IFoodManager _foodManager;

        public FoodViewManager(IFoodManager foodManager)
        {
            _foodManager = foodManager;
        }

        public IEnumerable<FoodView> GetAll()
        {
            return _foodManager.GetAll().Select(i => new FoodView(i));
        }
    }
}