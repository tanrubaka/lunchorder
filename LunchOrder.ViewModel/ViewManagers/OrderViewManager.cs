﻿using System.Collections.Generic;
using System.Linq;
using LunchOrder.DAL.IManagers;
using LunchOrder.ViewModel.Edits;
using LunchOrder.ViewModel.IViewManagers;
using LunchOrder.ViewModel.Views;

namespace LunchOrder.ViewModel.ViewManagers
{
    public class OrderViewManager : IOrderViewManager
    {
        private readonly IOrderManager _orderManager;

        public OrderViewManager(IOrderManager orderManager)
        {
            _orderManager = orderManager;
        }

        public OrderEdit GetEdit(int? id)
        {
            if (id == null || id == default(int))
            {
                return new OrderEdit();
            }
            return new OrderEdit(_orderManager.Get(id.Value));
        }

        public void Edit(OrderEdit model)
        {
            if (model.Id != default(int))
            {
                //edit method
            }
            else
            {
                _orderManager.Add(model);
            }
        }

        public IEnumerable<OrderView> GetAll()
        {
            return _orderManager.GetAll().Select(i => new OrderView(i));
        }
    }
}