﻿using LunchOrder.DAL.Models;

namespace LunchOrder.ViewModel.Edits
{
    public class OrderItemEdit : OrderItemInfo
    {
        public OrderItemEdit()
        {

        }

        public OrderItemEdit(OrderItemInfo obj) : base(obj)
        {

        }
    }
}