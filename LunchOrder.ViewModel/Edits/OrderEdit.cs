﻿using System.Collections.Generic;
using LunchOrder.DAL.Models;

namespace LunchOrder.ViewModel.Edits
{
    public class OrderEdit : OrderInfo
    {
        private List<OrderItemEdit> _orderItems;

        public List<OrderItemEdit> OrderItems
        {
            get => _orderItems??(_orderItems = new List<OrderItemEdit>());
            set => _orderItems = value;
        }

        public OrderEdit()
        {

        }

        public OrderEdit(OrderInfo obj) : base(obj)
        {

        }
    }
}