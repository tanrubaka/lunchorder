﻿using System;
using LunchOrder.ViewModel.IViewManagers;
using LunchOrder.ViewModel.ViewManagers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace LunchOrder.ViewModel
{
    public static class InitDependency
    {
        public static void Init(IServiceCollection services, IConfiguration configuration)
        {
            DAL.InitDependency.Init(services, configuration);

            services.AddSingleton<IFoodViewManager, FoodViewManager>();
            services.AddSingleton<IOrderViewManager, OrderViewManager>();
        }
    }
}
