﻿using LunchOrder.Models.Data;
using LunchOrder.Models.DomainObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace LunchOrder.DAL
{
    public static class InitDependency
    {
        public static void Init(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DataAccessSqlProvider"))
                );

            services.AddTransient<IRepository<Food>, EntityRepository<Food>>();
            services.AddTransient<IRepository<Order>, EntityRepository<Order>>();
            services.AddTransient<IRepository<OrderItem>, EntityRepository<OrderItem>>();
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
        }
    }
}