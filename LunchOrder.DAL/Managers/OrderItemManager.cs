﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchOrder.DAL.IManagers;
using LunchOrder.DAL.Models;
using LunchOrder.Models.Data;
using LunchOrder.Models.DomainObjects;

namespace LunchOrder.DAL.Managers
{
    public class OrderItemManager:IOrderItemManager
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderItemManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public OrderItemInfo Get(int id)
        {
            return GetInner(_unitOfWork.OrderItems.GetAll().Where(i => i.Id == id)).FirstOrDefault();
        }

        public IEnumerable<OrderItemInfo> GetByOrder(int orderId)
        {
            return GetInner(_unitOfWork.OrderItems.GetAll().Where(i=>i.OrderId == orderId)).ToList();
        }

        public int Add(OrderItemInfo model)
        {
            var newObject = new OrderItem
            {
                Qty = model.Qty,
                Price = model.Price,
                FoodId = model.FoodId,
                OrderId = model.OrderId
            };
            _unitOfWork.OrderItems.Add(newObject);
            _unitOfWork.SaveChanges();
            return newObject.Id;
        }

        private IQueryable<OrderItemInfo> GetInner(IQueryable<OrderItem> query)
        {
            return query.Select(i => new OrderItemInfo
            {
                Id = i.Id,
                FoodId = i.FoodId,
                OrderId = i.OrderId,
                FoodName = i.Food.Name,
                Qty = i.Qty,
                Price = i.Price
            });
        }
    }
}