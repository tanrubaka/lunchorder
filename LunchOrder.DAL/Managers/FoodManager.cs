﻿using System.Collections.Generic;
using System.Linq;
using LunchOrder.DAL.IManagers;
using LunchOrder.DAL.Models;
using LunchOrder.Models.Data;
using LunchOrder.Models.DomainObjects;

namespace LunchOrder.DAL.Managers
{
    public class FoodManager: IFoodManager
    {
        private readonly IUnitOfWork _unitOfWork;

        public FoodManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public FoodInfo Get(int id)
        {
            return GetInner(_unitOfWork.Foods.GetAll().Where(i=> i.Id == id)).FirstOrDefault();
        }

        public IEnumerable<FoodInfo> GetAll()
        {
            return GetInner(_unitOfWork.Foods.GetAll()).ToList();
        }

        private IQueryable<FoodInfo> GetInner(IQueryable<Food> query)
        {
            return query.Select(i => new FoodInfo
            {
                Id = i.Id,
                Name = i.Name,
                Description = i.Description,
                Price = i.Price
            });
        }
    }
}