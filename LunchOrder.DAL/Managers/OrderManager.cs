﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchOrder.DAL.IManagers;
using LunchOrder.DAL.Models;
using LunchOrder.Models.Data;
using LunchOrder.Models.DomainObjects;

namespace LunchOrder.DAL.Managers
{
    public class OrderManager: IOrderManager
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public OrderInfo Get(int id)
        {
            return GetInner(_unitOfWork.Orders.GetAll().Where(i => i.Id == id)).FirstOrDefault();
        }

        public IEnumerable<OrderInfo> GetAll()
        {
            return GetInner(_unitOfWork.Orders.GetAll()).ToList();
        }

        public int Add(OrderInfo model)
        {
            var newObject = new Order
            {
                Name = model.Name,
                Address = model.Address,
                Phone = model.Phone,
                OrderDate = DateTime.UtcNow,
                Amount = model.Amount
            };
            _unitOfWork.Orders.Add(newObject);
            _unitOfWork.SaveChanges();
            return newObject.Id;
        }

        private IQueryable<OrderInfo> GetInner(IQueryable<Order> query)
        {
            return query.Select(i => new OrderInfo
            {
                Id = i.Id,
                Name = i.Name,
                Phone = i.Phone,
                Address = i.Address,
                OrderDate = i.OrderDate,
                Amount = i.Amount
            });
        }
    }
}