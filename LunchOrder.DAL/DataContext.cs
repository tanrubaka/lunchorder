﻿using LunchOrder.Models.DomainObjects;
using Microsoft.EntityFrameworkCore;

namespace LunchOrder.DAL
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options):base(options)
        {
            
        }

        public DbSet<Food> Foods { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<OrderItem>()
                .HasOne(i => i.Order)
                .WithMany(i => i.Items)
                .HasForeignKey(i => i.OrderId);
        }
    }
}