﻿using System.Linq;
using LunchOrder.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace LunchOrder.DAL
{
    public class EntityRepository<T> : IRepository<T> where T : class, new()
    {
        public DataContext Context { get; set; }

        public EntityRepository(DataContext context)
        {
            Context = context;
        }

        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }


        public void UpdateProperty(T obj, string property)
        {
            Context.Entry(obj).Property(property).IsModified = true;
        }

        public void Attach(T obj)
        {
            Context.Set<T>().Attach(obj);
        }

        public void Delete(T obj)
        {
            Context.Entry(obj).State = EntityState.Deleted;
        }

        public void Add(T obj)
        {
            Context.Set<T>().Add(obj);
        }
    }
}