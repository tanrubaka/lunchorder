﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace LunchOrder.DAL.Migrations
{
    public partial class FillFood : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"SET IDENTITY_INSERT [dbo].[Foods] ON 
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (1, N'свекла свежая, капуста свежая, картофель, морковь, лук, филе куриное, томаты, чеснок, специи.', N'Борщ', CAST(30.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (2, NULL, N'Щи', CAST(28.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (3, N'говядина вк, ветчина, сервелат вк, лук, морковь, картофель, томаты, огурцы сс, оливки, лимон.', N'Солянка', CAST(50.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (4, N'филе говядины, лук, томаты, специи.', N'Гуляш из говядины', CAST(97.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (6, NULL, N'Котлета', CAST(50.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (7, N'рис, свинина, лук, морковь, чеснок, специи.', N'Плов со свининой', CAST(74.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (8, N'картофель, молоко, масло сливочное.', N'Картофельное пюре', CAST(30.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (9, N'ветчина, яйцо, огурец, морковь, горошек, картофель, майонез, лук, зелень.', N'Салат оливье', CAST(38.00 AS Decimal(18, 2)))
                GO
                INSERT [dbo].[Foods] ([Id], [Description], [Name], [Price]) VALUES (10, N'Выход: 1 кусочек.', N'Хлеб (кусочек)', CAST(3.00 AS Decimal(18, 2)))
                GO
                SET IDENTITY_INSERT [dbo].[Foods] OFF
                GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"delete [Foods] where Id in (1,2,3,4,5,6,7,8,9,10)");
        }
    }
}
