﻿using System;
using LunchOrder.Models.Data;
using LunchOrder.Models.DomainObjects;

namespace LunchOrder.DAL
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly DataContext _dataContext;

        public IRepository<Food> Foods { get; }
        public IRepository<Order> Orders { get; }
        public IRepository<OrderItem> OrderItems { get; }

        public UnitOfWork(DataContext dataContext,
            IRepository<Food> foods,
            IRepository<Order> orders,
            IRepository<OrderItem> orderItems)
        {
            _dataContext = dataContext;
            Foods = foods;
            Orders = orders;
            OrderItems = orderItems;
        }

        public bool SaveChanges()
        {
            try
            {
                _dataContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Discard()
        {
            _dataContext.Dispose();
        }
    }
}