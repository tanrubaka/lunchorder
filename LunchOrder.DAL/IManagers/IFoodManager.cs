﻿using System.Collections.Generic;
using LunchOrder.DAL.Models;

namespace LunchOrder.DAL.IManagers
{
    public interface IFoodManager
    {
        FoodInfo Get(int id);
        IEnumerable<FoodInfo> GetAll();
    }
}