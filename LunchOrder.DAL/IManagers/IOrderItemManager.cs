﻿using System.Collections.Generic;
using LunchOrder.DAL.Models;

namespace LunchOrder.DAL.IManagers
{
    public interface IOrderItemManager
    {
        OrderItemInfo Get(int id);
        IEnumerable<OrderItemInfo> GetByOrder(int orderId);
        int Add(OrderItemInfo model);
    }
}