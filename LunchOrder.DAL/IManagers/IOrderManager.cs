﻿using System.Collections.Generic;
using LunchOrder.DAL.Models;

namespace LunchOrder.DAL.IManagers
{
    public interface IOrderManager
    {
        OrderInfo Get(int id);
        IEnumerable<OrderInfo> GetAll();
        int Add(OrderInfo model);
    }
}