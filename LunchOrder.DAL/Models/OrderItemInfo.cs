﻿namespace LunchOrder.DAL.Models
{
    public class OrderItemInfo
    {
        public int Id { get; set; }

        public int Qty { get; set; }

        public double Price { get; set; }

        public int FoodId { get; set; }
        public string FoodName { get; set; }

        public int OrderId { get; set; }

        public OrderItemInfo()
        {
            
        }

        public OrderItemInfo(OrderItemInfo obj)
        {
            this.Id = obj.Id;
            this.Qty = obj.Qty;
            this.Price = obj.Price;
            this.FoodId = obj.FoodId;
            this.FoodName = obj.FoodName;
            this.OrderId = obj.OrderId;
        }
    }
}