﻿namespace LunchOrder.DAL.Models
{
    public class FoodInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public FoodInfo()
        {
            
        }

        public FoodInfo(FoodInfo obj)
        {
            this.Id = obj.Id;
            this.Name = obj.Name;
            this.Description = obj.Description;
            this.Price = obj.Price;
        }
    }
}