﻿using System;

namespace LunchOrder.DAL.Models
{
    public class OrderInfo
    {
        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public double Amount { get; set; }

        public OrderInfo()
        {
            
        }

        public OrderInfo(OrderInfo obj)
        {
            this.Id = obj.Id;
            this.OrderDate = obj.OrderDate;
            this.Name = obj.Name;
            this.Phone = obj.Phone;
            this.Address = obj.Address;
            this.Amount = obj.Amount;
        }
    }
}